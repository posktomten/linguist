#!/bin/bash
set -e
#set -x
CURRENT_VERSION=6.7.2
QT=Qt6
GLIBC=2.35

#HERE=$(pwd)
export LD_LIBRARY_PATH="${APPDIR}/usr/lib"
echo
#echo "APPDIR: ${APPDIR}"
#echo "I'm here: ${HERE}"
#echo "ld_library_path ${LD_LIBRARY_PATH}"
echo "languagetool includes \"lconvert\", \"linguist\", \"lrelease\" and \"lupdate\"."
echo "Copyright (C) The Qt Company."
echo "Packaged in an AppImage by Ingemar Ceicer."
echo "Qt version:    ${CURRENT_VERSION}"
echo "GLIBC version: ${GLIBC}"
echo "https://gitlab.com/posktomten/linguist"
echo

PS3='Please enter your choice: '
options=("lconvert" "linguist" "lrelease" "lupdate" "Check for updates" "Quit")

select opt in "${options[@]}"; do
    case $opt in
    "lconvert")
        echo "Enter the arguments to lconvert (-h for help)"
        echo -n ":> "
        read input
        ${APPDIR}/usr/bin/lconvert $input
        exit 0
        ;;
    "linguist")
        ${APPDIR}/usr/bin/linguist &
        exit 0
        ;;
    "lrelease")
        echo "Enter the arguments to lrelease (-h for help)"
        echo -n ":> "
        read input
        ${APPDIR}/usr/bin/lrelease $input
         exit 0
        ;;
    "lupdate")
        echo "Enter the arguments to lupdate (-h for help)"
         echo -n ":> "
        read input
        ${APPDIR}/usr/bin/lupdate $input
        exit 0
        ;;
    "Check for updates")
        echo "Requires curl to be installed"
        LATEST_VERSION=$(/usr/bin/curl -s https://bin.ceicer.com/linguist/bin/linux/GLIBC${GLIBC}/languagetools/${QT}/.version)
        echo "This version:   Qt${CURRENT_VERSION}"
        echo "Latest version: Qt${LATEST_VERSION}"
        exit 0
        ;;
    "Quit")
        echo "Goodbye!"
        exit 0
        ;;
    *) echo "Invalid option $REPLY"
    ;;
    esac
done
