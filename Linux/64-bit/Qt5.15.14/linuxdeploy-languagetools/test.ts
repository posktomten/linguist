<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Cli</name>
    <message>
        <location filename="../cli.cpp" line="98"/>
        <source>Compiler:</source>
        <translation>Kompilator:</translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Development tool: Qt</source>
        <translation>Utvecklingsverktyg: Qt</translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Compiled with: </source>
        <translation>Kompilerad med: </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Copyright (C) </source>
        <translation>Copyright (C) </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Website: </source>
        <translation>Hemsida: </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Swedish website: </source>
        <translation>Svensk hemsida: </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Source code: </source>
        <translation>Källkod: </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>E-mail: </source>
        <translation>E-post: </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>This program was created </source>
        <translation>Detta program skapades </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Path: </source>
        <translation>Sökväg: </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>Runs on: </source>
        <translation>Körs på: </translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="111"/>
        <source>streamCapture2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.</source>
        <translation>streamCapture2 är fri programvara: du kan distribuera den och/eller modifiera den under villkoren i GNU General Public License som publicerats av Free Software Foundation, version 3. Detta program distribueras i hopp om att det ska vara användbart, men UTAN NÅGRA GARANTI; utan ens underförstådd garanti för SÄLJBARHET eller LÄMPLIGHET FÖR ETT SÄRSKILT SYFTE. Se GNU General Public License för mer information.</translation>
    </message>
</context>
<context>
    <name>DownloadListDialog</name>
    <message>
        <location filename="../downloadlistdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="23"/>
        <source>Please zoom using the keyboard. &quot;Ctrl + +&quot;, &quot;Ctrl + -&quot; or &quot;Ctrl + 0&quot;.</source>
        <translation>Zooma med tangentbordet. &quot;Ctrl + +&quot;, &quot;Ctrl + -&quot; eller &quot;Ctrl + 0&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="46"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="63"/>
        <location filename="../downloadlistdialog.cpp" line="120"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="80"/>
        <source>Save as...</source>
        <translation>Spara som...</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="121"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="122"/>
        <source>Save as text file</source>
        <translation>Spara som textfil</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="131"/>
        <location filename="../downloadlistdialog.cpp" line="134"/>
        <location filename="../downloadlistdialog.cpp" line="141"/>
        <source>Text file</source>
        <translation>Textfil</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="131"/>
        <location filename="../downloadlistdialog.cpp" line="134"/>
        <source>Text file, optional extension</source>
        <translation>Textfil, valfritt filtillägg</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="155"/>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation>Det gick inte att spara filen.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="253"/>
        <location filename="../downloadlistdialog.cpp" line="294"/>
        <source>Unable to find file</source>
        <translation>Kan inte hitta filen</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="254"/>
        <location filename="../downloadlistdialog.cpp" line="295"/>
        <source>Unable to find</source>
        <translation>Kan inte hitta</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="156"/>
        <location filename="../downloadlistdialog.cpp" line="255"/>
        <location filename="../downloadlistdialog.cpp" line="296"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="1278"/>
        <location filename="../newprg.cpp" line="1303"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1279"/>
        <location filename="../newprg.cpp" line="1304"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1372"/>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl kraschade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1378"/>
        <source>Could not stop svtplay-dl.</source>
        <translation>Kunde inte stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1389"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Ta bort alla filer som kan ha blivit nedladdade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1386"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl stoppade. Exit kod </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1412"/>
        <source>Copy to: </source>
        <translation>Kopiera till: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1577"/>
        <source>Download to: </source>
        <translation>Ladda ner till: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../language.cpp" line="36"/>
        <location filename="../language.cpp" line="90"/>
        <location filename="../language.cpp" line="142"/>
        <location filename="../newprg.cpp" line="93"/>
        <location filename="../theme.cpp" line="38"/>
        <location filename="../theme.cpp" line="93"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="38"/>
        <location filename="../language.cpp" line="92"/>
        <location filename="../language.cpp" line="144"/>
        <source>Change language on next start</source>
        <translation>Ändra språk vid nästa stsrt</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="47"/>
        <location filename="../language.cpp" line="101"/>
        <location filename="../language.cpp" line="153"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="132"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="400"/>
        <location filename="../download_runtime.cpp" line="29"/>
        <location filename="../download.cpp" line="370"/>
        <location filename="../download.cpp" line="562"/>
        <location filename="../downloadall.cpp" line="159"/>
        <location filename="../downloadall.cpp" line="290"/>
        <location filename="../downloadall.cpp" line="336"/>
        <location filename="../downloadallepisodes.cpp" line="39"/>
        <location filename="../downloadallepisodes.cpp" line="56"/>
        <location filename="../downloadallepisodes.cpp" line="73"/>
        <location filename="../language.cpp" line="40"/>
        <location filename="../language.cpp" line="94"/>
        <location filename="../language.cpp" line="146"/>
        <location filename="../newprg.cpp" line="97"/>
        <location filename="../newprg.cpp" line="280"/>
        <location filename="../newprg.cpp" line="388"/>
        <location filename="../newprg.cpp" line="1433"/>
        <location filename="../newprg.cpp" line="1498"/>
        <location filename="../newprg.cpp" line="1761"/>
        <location filename="../newprg.cpp" line="2214"/>
        <location filename="../newprg.cpp" line="2285"/>
        <location filename="../paytv_create.cpp" line="69"/>
        <location filename="../paytv_create.cpp" line="105"/>
        <location filename="../paytv_create.cpp" line="140"/>
        <location filename="../paytv_create.cpp" line="157"/>
        <location filename="../paytv_edit.cpp" line="31"/>
        <location filename="../paytv_edit.cpp" line="146"/>
        <location filename="../paytv_edit.cpp" line="179"/>
        <location filename="../paytv_edit.cpp" line="196"/>
        <location filename="../paytv_edit.cpp" line="240"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_create.cpp" line="38"/>
        <location filename="../st_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="127"/>
        <location filename="../st_edit.cpp" line="143"/>
        <location filename="../test_translation.cpp" line="40"/>
        <location filename="../theme.cpp" line="42"/>
        <location filename="../theme.cpp" line="97"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="395"/>
        <source>Up and running</source>
        <oldsource>Upp and running</oldsource>
        <translation>Uppe och kör</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="154"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="78"/>
        <source>Open the folder where streamCapture2 is located</source>
        <translation>Öppna mappen där streamCapture2 finns</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="82"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="86"/>
        <source>Read message from the developer (if there is any)</source>
        <translation>Läs meddelande från utvecklaren (om det finns något)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="91"/>
        <source>Force update</source>
        <translation>Tvinga en uppdatering</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="107"/>
        <source>streamCapture2 is not allowed to open </source>
        <translation>streamCapture2 får inte öppna </translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="107"/>
        <source> Use the file manager instead.</source>
        <translation> Använd filhanteraren i stället.</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="37"/>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="39"/>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="155"/>
        <source>Update this AppImage to the latest version</source>
        <translation>Uppdatera den här AppImagen till senaste version</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="342"/>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation>Klicka på &quot;Verktyg&quot; och &quot;Underhållsverktyg...&quot;</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="463"/>
        <source>Resolution</source>
        <translation>Upplösning</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="467"/>
        <source>More then 1080p</source>
        <translation>Mer än 1080p</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="534"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inte ett körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ladda ner en AppImage som innehåller FFmpeg.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="545"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inget körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ladda ner en portable streamCapture2 som innehåller FFmpeg.&lt;br&gt;&lt;br&gt;Eller ladda ner och placera ffmpeg.exe i samma mapp som streamcapture2.exe.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="548"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inte ett körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="710"/>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation>Det gick inte att spara en fil för att lagra listan med senaste sökningar.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="735"/>
        <source>Could not save a file to store the list of downloads.
Check your file permissions.</source>
        <translation>Det gick inte att spara en fil för att lagra nedladdningslistan.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="196"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>Begäran behandlas...
Förbereder sig för att ladda ner...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="314"/>
        <location filename="../downloadall.cpp" line="205"/>
        <source>Selected folder to copy to is </source>
        <translation>Vald mapp att kopiera till är </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="610"/>
        <location filename="../listallepisodes.cpp" line="70"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Begäran behandlas ...
Startar att söka...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="84"/>
        <location filename="../nfo.cpp" line="52"/>
        <location filename="../sok.cpp" line="122"/>
        <source>The search field is empty!</source>
        <translation>Sökrutan är tom!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="92"/>
        <location filename="../sok.cpp" line="128"/>
        <source>Incorrect URL</source>
        <translation>Felaktig URL</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2127"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Det går inte att hitta några videoströmmar , kontrollera adressen.

</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2062"/>
        <source>Downloading...</source>
        <translation>Laddar ner...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="396"/>
        <source>Starts downloading: </source>
        <translation>Startar nedladdningen: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="405"/>
        <location filename="../downloadall.cpp" line="547"/>
        <location filename="../downloadallepisodes.cpp" line="351"/>
        <source>Unable to find any streams with the selected video resolution.</source>
        <translation>Det gick inte att hitta några strömmar med den valda videoupplösningen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="441"/>
        <location filename="../downloadall.cpp" line="574"/>
        <location filename="../downloadallepisodes.cpp" line="382"/>
        <source>Removing old files, if there are any...</source>
        <translation>Tar bort gamla filer, om det finns några...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="462"/>
        <location filename="../download.cpp" line="465"/>
        <source>The download failed.</source>
        <translation>Nedladdningen misslyckades.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="564"/>
        <location filename="../downloadall.cpp" line="338"/>
        <source>Save as...</source>
        <translation>Spara som...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="568"/>
        <location filename="../downloadall.cpp" line="342"/>
        <source>Select file name</source>
        <translation>Välj filnamn</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="589"/>
        <location filename="../downloadallepisodes.cpp" line="443"/>
        <source>No folder is selected</source>
        <translation>Ingen mapp är vald</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="323"/>
        <location filename="../listallepisodes.cpp" line="163"/>
        <location filename="../sok.cpp" line="155"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="30"/>
        <location filename="../downloadall.cpp" line="34"/>
        <source>cannot be found or is not an executable program.</source>
        <translation>kan inte hittas eller är inte ett körbart program.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.exe.</source>
        <translation>Klicka på &quot;Verktyg&quot; och välj svtplay-dl.exe.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="164"/>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation>Du har valt att ladda ner mer än en fil med samma namn. För att inte skriva över filer skapas mappar för varje fil.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="234"/>
        <source>The video streams are saved in</source>
        <translation>Videoströmmarna sparas i</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="501"/>
        <location filename="../downloadall.cpp" line="506"/>
        <source>The download failed.
If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation>Nedladdningen misslyckades.
Om ett användarnamn och lösenord eller &apos;st&apos; cookie krävs måste du ange dessa.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <location filename="../downloadallepisodes.cpp" line="29"/>
        <location filename="../listallepisodes.cpp" line="43"/>
        <location filename="../newprg.cpp" line="606"/>
        <location filename="../setgetconfig.cpp" line="603"/>
        <location filename="../sok.cpp" line="59"/>
        <source> cannot be found or is not an executable program.</source>
        <translation> kan inte hittas eller är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="35"/>
        <location filename="../downloadallepisodes.cpp" line="30"/>
        <location filename="../listallepisodes.cpp" line="44"/>
        <location filename="../setgetconfig.cpp" line="604"/>
        <location filename="../sok.cpp" line="60"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Vänligen klicka på &quot;Verktyg&quot; och välj svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="32"/>
        <source>Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>Klicka på &quot;Verktyg och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="104"/>
        <source>The request is processed...</source>
        <translation>Begäran behandlas...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="104"/>
        <source>Preparing to download...</source>
        <translation>Förbereder för nedladdning...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="121"/>
        <source>Download streaming media to folder</source>
        <translation>Laddar ner strömmande media till mapp</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="240"/>
        <source>The video stream is saved in </source>
        <oldsource>The video file is saved in </oldsource>
        <translation>Videoströmmen sparas i </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="249"/>
        <location filename="../downloadall.cpp" line="64"/>
        <location filename="../downloadallepisodes.cpp" line="209"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att ladda ner videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="267"/>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation>Du har inte rätt att spara i standardmappen.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="289"/>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Mappen för nedladdning av videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="214"/>
        <location filename="../download.cpp" line="303"/>
        <location filename="../downloadall.cpp" line="78"/>
        <location filename="../downloadall.cpp" line="113"/>
        <location filename="../downloadall.cpp" line="199"/>
        <location filename="../downloadallepisodes.cpp" line="223"/>
        <location filename="../downloadallepisodes.cpp" line="256"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Du har inte rätten att spara i mappen du valt.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="464"/>
        <source>Less then 720p</source>
        <translation>Mindre än 720p</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="465"/>
        <source>Less than or equal to 720p</source>
        <translation>Mindre än eller lika med 720p</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="466"/>
        <source>Less than or equal to 1080p</source>
        <translation>Mindre än eller lika med 1080p</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="82"/>
        <location filename="../downloadall.cpp" line="434"/>
        <location filename="../downloadallepisodes.cpp" line="113"/>
        <location filename="../listallepisodes.cpp" line="123"/>
        <location filename="../sok.cpp" line="87"/>
        <source>You need to copy the TV4 token from your browser and save it in streamCapture2.</source>
        <translation>Du måste kopiera TV4-token från din webbläsare och spara den i streamCapture2.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="437"/>
        <location filename="../downloadall.cpp" line="570"/>
        <location filename="../downloadallepisodes.cpp" line="376"/>
        <source>Merge audio and video...</source>
        <translation>Slår samman ljud och video...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="476"/>
        <location filename="../download.cpp" line="485"/>
        <location filename="../downloadall.cpp" line="470"/>
        <location filename="../downloadall.cpp" line="477"/>
        <source>Download succeeded</source>
        <translation>Nedladdningen lyckades</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="542"/>
        <location filename="../download.cpp" line="547"/>
        <source>The download failed </source>
        <translation>Nedladdningen misslyckades </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="187"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att kopiera videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="523"/>
        <location filename="../download.cpp" line="528"/>
        <location filename="../downloadall.cpp" line="516"/>
        <location filename="../downloadall.cpp" line="524"/>
        <location filename="../downloadallepisodes.cpp" line="423"/>
        <location filename="../downloadallepisodes.cpp" line="426"/>
        <location filename="../downloadallepisodes.cpp" line="431"/>
        <source>Download completed</source>
        <translation>Nedladdningen klar</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="38"/>
        <location filename="../downloadallepisodes.cpp" line="55"/>
        <location filename="../downloadallepisodes.cpp" line="72"/>
        <source>Download anyway</source>
        <translation>Ladda ner i all fall</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="45"/>
        <location filename="../downloadallepisodes.cpp" line="62"/>
        <location filename="../downloadallepisodes.cpp" line="79"/>
        <source>You have chosen to create folders for each file and have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Du har valt att skapa mappar för varje fil och kopiera filerna.
Tyvärr är detta inte möjligt eftersom streamCapture2 inte känner till filnamnen.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="176"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control.
If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation>När svtplay-dl har börjat ladda ner alla avsnitt har streamCapture2 inte längre kontroll.
Om du vill avbryta kan du behöva logga ut eller starta om datorn.
Du kan försöka avbryta med kommandot
&quot;sudo killall python3&quot;

Vill du starta nedladdningen?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="234"/>
        <source>Download all episodes to folder</source>
        <translation>Ladda ner alla avsnitt till mappen</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="342"/>
        <source>Starts downloading all episodes: </source>
        <translation>Börjar ladda ner alla avsnitt: </translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="388"/>
        <source>The download failed. Did you forget to enter username and password?</source>
        <translation>Går inte att logga in. Har du glömt att ange användarnamn och lösenord?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="402"/>
        <location filename="../nfo.cpp" line="161"/>
        <source>Episode</source>
        <translation>Avsnitt</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="402"/>
        <source>of</source>
        <translation>av</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="416"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Mediefilerna (och om du har valt undertexterna) har laddats ner.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="421"/>
        <source>Preparing to download</source>
        <translation>Förbereder att ladda ner</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="608"/>
        <location filename="../download.cpp" line="609"/>
        <location filename="../listallepisodes.cpp" line="68"/>
        <location filename="../listallepisodes.cpp" line="69"/>
        <source>Searching...</source>
        <translation>Söker...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="354"/>
        <location filename="../sok.cpp" line="197"/>
        <source> crashed.</source>
        <translation> kraschade.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="356"/>
        <location filename="../sok.cpp" line="199"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Kan inte hitta någon videoström, kontrollera adressen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="621"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="624"/>
        <source>bold and italic</source>
        <translation>fet och kursiv</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="627"/>
        <source>bold</source>
        <translation>fet</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="629"/>
        <source>italic</source>
        <translation>kursivt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="633"/>
        <source>Current font:</source>
        <translation>Nuvarande teckensnitt:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="634"/>
        <source>size:</source>
        <translation>storlek:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="683"/>
        <source>View download list</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="734"/>
        <source>The list of video streams to download will be deleted and can not be restored.
Do you want to continue?</source>
        <translation>Listan över videoströmmar som ska laddas ned raderas och kan inte återställas.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="856"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1432"/>
        <location filename="../newprg.cpp" line="1497"/>
        <location filename="../newprg.cpp" line="1760"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="91"/>
        <location filename="../newprg.cpp" line="1499"/>
        <source>Download streaming media to directory</source>
        <translation>Ladda ner strömmande media till mappen</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1707"/>
        <location filename="../newprg.cpp" line="1717"/>
        <source>Cannot find </source>
        <translation>Kan inte hitta </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1762"/>
        <location filename="../newprg.cpp" line="1778"/>
        <source>Select svtplay-dl</source>
        <translation>Välj svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1778"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1797"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation>svtplay-dl är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2149"/>
        <source>The search is complete</source>
        <translation>Sökningen är klar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2151"/>
        <source>The search failed</source>
        <translation>Sökningen misslyckades</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2231"/>
        <source>Edit saved searches...</source>
        <translation>Redigera sparade sökningar...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="972"/>
        <source>Path to svtplay-dl: </source>
        <translation>Sökväg till svtplay-dl: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="976"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation>Sökväg till svtplay-dl.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="947"/>
        <source>svtplay-dl is in the system path.</source>
        <translation>svtplay-dl finns i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="95"/>
        <source>Change settings on next start</source>
        <translation>Ändra inställningar vid nästa start</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="104"/>
        <source>The program must be restarted for the new settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya inställningarna ska träda i kraft.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="281"/>
        <source>Open downloaded file</source>
        <translation>Öppna nedladdad fil</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="284"/>
        <source>Video files</source>
        <translation>Video filer</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="389"/>
        <source>Enter TV4 token</source>
        <translation>Kopiera in TV4 tecknen</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="392"/>
        <source>You must paste the exact TV4 token.</source>
        <translation>Du måste klistra in precis rätt tecken.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="541"/>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation>svtplay-dl hittas inte i systemsökvägen.
Du kan ladda ner vilken svtplay-dl du vill.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="607"/>
        <source>Please click on &quot;svtplay-dl&quot; and select svtplay-dl...</source>
        <translation>Klicka på &quot;svtplay-dl&quot; och välj svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="682"/>
        <location filename="../newprg.cpp" line="713"/>
        <source>Video stream</source>
        <translation>Videoström</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="682"/>
        <location filename="../newprg.cpp" line="715"/>
        <source>Video streams</source>
        <translation>Videoströmmar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="718"/>
        <source>Edit download list</source>
        <translation>Redigera nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="851"/>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation>Klicka på &quot;Verktyg&quot; och &quot;Uppdatera&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="904"/>
        <source>A graphical shell for </source>
        <translation>Ett grafiskt skal för </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="906"/>
        <source> and </source>
        <translation> och </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="912"/>
        <source>Many thanks to </source>
        <translation>Stort tack till </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="952"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation>svtplay-dl.exe finns i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="958"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation>FEL! svtplay-dl finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="962"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation>FEL! svtplay-dl.exe finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="983"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation>FEL! svtplay-dl kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="987"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation>FEL! svtplay-dl.exe kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1004"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation>FEL! FFmpeg kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1434"/>
        <source>Copy streaming media to directory</source>
        <translation>Kopiera mediafilen till mapp</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1947"/>
        <location filename="../newprg.cpp" line="1971"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation>Misslyckades med att skapa en genväg på skrivbordet.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="39"/>
        <location filename="../st_edit.cpp" line="144"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation>Ange &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="41"/>
        <location filename="../st_edit.cpp" line="147"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation>Ange &apos;st&apos;-cookien som vidioströmleverantören har sparat i din webbläsare.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="337"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="859"/>
        <location filename="../setgetconfig.cpp" line="345"/>
        <source>Download a new</source>
        <translation>Ladda ner en ny</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="909"/>
        <source> streamCapture2 handles downloads of video streams.</source>
        <translation> streamCapture2 hanterar nedladdningar av videoströmmar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="912"/>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation> för den italienska översättningen. Och för många bra idéer som har gjort programmet bättre.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1007"/>
        <source>Path to ffmpeg.exe: </source>
        <translation>Sökvägen till ffmpeg.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1010"/>
        <source>Path to ffmpeg: </source>
        <translation>Sökvägen till ffmpeg: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1455"/>
        <location filename="../newprg.cpp" line="1520"/>
        <source>You do not have the right to save to</source>
        <translation>Du har inte rättighet att spara i</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1455"/>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation>och den kan inte användas som standardmapp att kopiera till.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1520"/>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation>och den kan inte användas som standardmapp för nedladdningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1707"/>
        <location filename="../newprg.cpp" line="1717"/>
        <source> in system path.</source>
        <translation> i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1800"/>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation>svtplay-dl.exe är inte ett körbart program.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1833"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Click &quot;svtplay-dl&quot;, &quot;Select svtplay-dl...&quot;
to select svtplay-dl.</source>
        <translation>svtplay-dl kan inte hittas eller är inte ett körbart program.
Klicka på &quot;svtplay-dl&quot;, &quot;Välj svtplay-dl...&quot;
för att välja svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1873"/>
        <source>here</source>
        <translation>här</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1873"/>
        <source>You can find more versions</source>
        <translation>Du kan hitta fler versioner</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1906"/>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation>Underhållsverktyget kan inte hittas.
Endast om du installerar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1908"/>
        <source>is it possible to update and uninstall the program.</source>
        <translation>är det möjligt att uppdatera och avinstallera programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2158"/>
        <source>Select</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2179"/>
        <source>Click to copy to the search box.</source>
        <translation>Klicka för att kopiera till sökrutan.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2203"/>
        <source>The number of previous searches to be saved...</source>
        <oldsource>The number of recently opened files to be displayed...</oldsource>
        <translation>Antalet tidigare sökningar som ska sparas...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2205"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Bestäm antalet tidigare sökningar som ska sparas. Om antalet sökningar överstiger det angivna antalet kommer den äldsta sökningen att tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2240"/>
        <source>Edit saved searches</source>
        <translation>Redigera sparade sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2291"/>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation>Alla dina sparade inställningar raderas.
Alla listor över filer som ska laddas ned försvinner.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2335"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation>Det gick inte att ta bort konfigurationsfilerna.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2349"/>
        <source>was normally terminated. Exit code = </source>
        <translation>avslutades normalt. Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2353"/>
        <source>crashed. Exit code = </source>
        <translation>kraschade. Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2234"/>
        <source>Click to edit all saved searches.</source>
        <translation>Klicka för att redigera alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="168"/>
        <source>Default Style</source>
        <translation>Standardstil</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="317"/>
        <source>If the download does not work</source>
        <translation>Om nedladdningen inte fungerar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="490"/>
        <source>Edit streamCapture2 settings</source>
        <translation>Redigera inställningarna för streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="515"/>
        <source>Edit download svtplay-dl settings</source>
        <translation>Redigera inställningarna för ladda ner svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1629"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Go to &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-d.exe kan inte hittas eller är inte ett körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1640"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl.exe kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1676"/>
        <source>svtplay-dl cannot be found or is not an executable program.
 Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1685"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1873"/>
        <source>This is a BETA version. This AppImage can&apos;t be updated.</source>
        <translation>Detta är en BETA-version. Den här AppImage kan inte uppdateras.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2055"/>
        <source>Best quality is selected automatically</source>
        <translation>Bästa kvalite väljs automatiskt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2056"/>
        <source>Best method is selected automatically</source>
        <translation>Bästa metod väljs automatiskt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2247"/>
        <source>Remove all saved searches</source>
        <translation>Ta bort alla sparade sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2250"/>
        <source>Click to delete all saved searches.</source>
        <translation>Klicka för att ta bort alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2221"/>
        <source>The number of searches to be saved: </source>
        <oldsource>Set number of Recent Files: </oldsource>
        <translation>Ange maxantalet sparade sökningar: </translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="106"/>
        <location filename="../paytv_edit.cpp" line="148"/>
        <source>Enter your username</source>
        <translation>Skriv in ditt användarnamn</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="371"/>
        <location filename="../downloadall.cpp" line="291"/>
        <location filename="../downloadallepisodes.cpp" line="322"/>
        <location filename="../listallepisodes.cpp" line="162"/>
        <location filename="../paytv_create.cpp" line="141"/>
        <location filename="../paytv_edit.cpp" line="182"/>
        <location filename="../sok.cpp" line="154"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="55"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="79"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="94"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="250"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="271"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="288"/>
        <location filename="../coppytodefaultlocation.cpp" line="39"/>
        <location filename="../download.cpp" line="83"/>
        <location filename="../download.cpp" line="218"/>
        <location filename="../download.cpp" line="253"/>
        <location filename="../download.cpp" line="271"/>
        <location filename="../download.cpp" line="293"/>
        <location filename="../download.cpp" line="307"/>
        <location filename="../download.cpp" line="369"/>
        <location filename="../download.cpp" line="563"/>
        <location filename="../downloadall.cpp" line="68"/>
        <location filename="../downloadall.cpp" line="82"/>
        <location filename="../downloadall.cpp" line="117"/>
        <location filename="../downloadall.cpp" line="156"/>
        <location filename="../downloadall.cpp" line="191"/>
        <location filename="../downloadall.cpp" line="200"/>
        <location filename="../downloadall.cpp" line="289"/>
        <location filename="../downloadall.cpp" line="337"/>
        <location filename="../downloadall.cpp" line="435"/>
        <location filename="../downloadallepisodes.cpp" line="114"/>
        <location filename="../downloadallepisodes.cpp" line="213"/>
        <location filename="../downloadallepisodes.cpp" line="226"/>
        <location filename="../downloadallepisodes.cpp" line="260"/>
        <location filename="../listallepisodes.cpp" line="124"/>
        <location filename="../newprg.cpp" line="387"/>
        <location filename="../newprg.cpp" line="542"/>
        <location filename="../newprg.cpp" line="1456"/>
        <location filename="../newprg.cpp" line="1523"/>
        <location filename="../newprg.cpp" line="1633"/>
        <location filename="../newprg.cpp" line="1643"/>
        <location filename="../newprg.cpp" line="1678"/>
        <location filename="../newprg.cpp" line="1686"/>
        <location filename="../newprg.cpp" line="1710"/>
        <location filename="../newprg.cpp" line="1720"/>
        <location filename="../newprg.cpp" line="1804"/>
        <location filename="../newprg.cpp" line="1838"/>
        <location filename="../newprg.cpp" line="1874"/>
        <location filename="../newprg.cpp" line="1911"/>
        <location filename="../newprg.cpp" line="1950"/>
        <location filename="../newprg.cpp" line="1974"/>
        <location filename="../newprg.cpp" line="2213"/>
        <location filename="../newprg.cpp" line="2337"/>
        <location filename="../nfo.cpp" line="68"/>
        <location filename="../nfo.cpp" line="80"/>
        <location filename="../nfo.cpp" line="92"/>
        <location filename="../nfo.cpp" line="107"/>
        <location filename="../nfo.cpp" line="186"/>
        <location filename="../paytv_create.cpp" line="68"/>
        <location filename="../paytv_create.cpp" line="104"/>
        <location filename="../paytv_create.cpp" line="139"/>
        <location filename="../paytv_edit.cpp" line="145"/>
        <location filename="../paytv_edit.cpp" line="178"/>
        <location filename="../paytv_edit.cpp" line="239"/>
        <location filename="../setgetconfig.cpp" line="108"/>
        <location filename="../setgetconfig.cpp" line="537"/>
        <location filename="../setgetconfig.cpp" line="550"/>
        <location filename="../setgetconfig.cpp" line="714"/>
        <location filename="../setgetconfig.cpp" line="738"/>
        <location filename="../shortcuts.cpp" line="68"/>
        <location filename="../shortcuts.cpp" line="143"/>
        <location filename="../sok.cpp" line="88"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="126"/>
        <location filename="../st_edit.cpp" line="142"/>
        <location filename="../user_message.cpp" line="115"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="374"/>
        <location filename="../downloadall.cpp" line="294"/>
        <location filename="../paytv_create.cpp" line="108"/>
        <location filename="../paytv_create.cpp" line="144"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <location filename="../paytv_edit.cpp" line="184"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation>Mellanslag är inte tillåtna. Använd endast tecken
som din leverantör godkänner.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="70"/>
        <location filename="../paytv_edit.cpp" line="241"/>
        <location filename="../st_create.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="128"/>
        <source>Streaming service</source>
        <translation>Strömningstjänst</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="72"/>
        <location filename="../paytv_edit.cpp" line="243"/>
        <location filename="../st_create.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="131"/>
        <source>Enter the name of your streaming service.</source>
        <translation>Ange namnet på din strömningstjänst.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="162"/>
        <location filename="../paytv_edit.cpp" line="201"/>
        <source>Save password?</source>
        <translation>Spara lösenordet?</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="163"/>
        <location filename="../paytv_edit.cpp" line="202"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vill du spara lösenordet (osäkert)?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="180"/>
        <location filename="../newprg.cpp" line="727"/>
        <location filename="../newprg.cpp" line="2284"/>
        <location filename="../paytv_create.cpp" line="156"/>
        <location filename="../paytv_edit.cpp" line="195"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="181"/>
        <location filename="../newprg.cpp" line="729"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Manage Login details for </source>
        <translation>Hantera inloggningsuppgifter för </translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="27"/>
        <source>Edit, rename or delete
</source>
        <translation>Redigera, byt namn eller ta bort
</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Rename</source>
        <translation>Byt namn</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Delete</source>
        <oldsource>Delet</oldsource>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="58"/>
        <source>Create New</source>
        <translation>Skapa ny</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="68"/>
        <location filename="../setgetconfig.cpp" line="470"/>
        <source>No Password</source>
        <translation>Inget lösenord</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="36"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Du har inte valt någon plats att kopiera mediafilerna till.
Var vänlig och bestäm en plats innan du fortsätter.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="63"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation>Det finns redan en fil med samma namn. Filen kopieras inte.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="71"/>
        <location filename="../coppytodefaultlocation.cpp" line="97"/>
        <source>Copy succeeded</source>
        <translation>Kopieringen lyckades</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="76"/>
        <location filename="../coppytodefaultlocation.cpp" line="102"/>
        <source>Copy failed</source>
        <translation>Kopieringen misslyckades</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="29"/>
        <source>The mission failed!</source>
        <translation>Uppdraget misslyckades!</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="32"/>
        <source>Mission accomplished!</source>
        <translation>Uppdrag slutfört!</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="67"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas i
&quot;~/.local/share/applikationer&quot;
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="118"/>
        <location filename="../shortcuts.cpp" line="120"/>
        <source>Download video streams.</source>
        <translation>Ladda ner videoströmmar.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="142"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="279"/>
        <location filename="../test_translation.cpp" line="39"/>
        <source>Open</source>
        <translation>Öppna</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="41"/>
        <source>Open your language file</source>
        <translation>Öppna din språkfil</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="44"/>
        <source>Compiled language file</source>
        <translation>Kompilerad språkfil</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="25"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation>Hantera &apos;st&apos; cookie för </translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="27"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation>Använd, Använd inte, Redigera eller Ta bort
&apos;st&apos; cookie för
</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use</source>
        <translation>Använd</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Do not use</source>
        <translation>Använd inte</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="202"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ställ in en ny &apos;st&apos;-cookie</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="203"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation>Klistra in och spara &apos;st&apos; cookien som din streamingleverantör har laddat ner till din webbläsare.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="68"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation>Kvalitet, Metod, Codec, Upplösning, Språk och Roll</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="204"/>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation>Går inte att logga in. Har du glömt att ange användarnamn och lösenord?</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="286"/>
        <source>ERROR: No videos found. Can&apos;t find video info.</source>
        <translation>FEL: Hittar ingen video. Kan inte hitta information om videon.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="210"/>
        <source>ERROR: No videos found. You need a token to access the website. see https://svtplay-dl.se/tv4play/</source>
        <translation>FEL: Inga videor hittades. Du behöver en token för att komma åt webbplatsen. se https://svtplay-dl.se/tv4play/</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="215"/>
        <source>ERROR: That site is not supported. Please visit https://github.com/spaam/svtplay-dl/issues</source>
        <translation>FEL: Den webbplatsen stöds inte. Besök https://github.com/spaam/svtplay-dl/issues</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="220"/>
        <source>ERROR: No videos found. This mode is not supported anymore. Need the url with the video.</source>
        <translation>FEL: Inga videor hittades. Behöver webbadressen med videon.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="225"/>
        <source>ERROR: No videos found. We can&apos;t download DRM protected content from this site.</source>
        <translation>FEL: Inga videor hittades. Vi kan inte ladda ner DRM-skyddat innehåll från den här webbplatsen.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="374"/>
        <source>ERROR: No videos found. Media doesn&apos;t have any associated videos.</source>
        <translation>FEL: Inga videor hittades. Media har inga associerade videor.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="383"/>
        <source>ERROR: No videos found. Can&apos;t find audio info.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta ljudinformation.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="391"/>
        <source>ERROR: No videos found. Cant find video id.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta video-id.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="223"/>
        <location filename="../listallepisodes.cpp" line="232"/>
        <location filename="../listallepisodes.cpp" line="250"/>
        <location filename="../sok.cpp" line="280"/>
        <location filename="../sok.cpp" line="292"/>
        <location filename="../sok.cpp" line="298"/>
        <location filename="../sok.cpp" line="304"/>
        <source>ERROR: No videos found. Can&apos;t find video id for the video.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta video-id för videon.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="259"/>
        <source>ERROR: No videos found. Can&apos;t find video id.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta videons id.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="277"/>
        <location filename="../sok.cpp" line="316"/>
        <source>ERROR: No videos found. Use the video page not the series page.</source>
        <translation>FEL: Inga videor hittades. Använd videosidan inte seriesidan.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="286"/>
        <location filename="../sok.cpp" line="322"/>
        <source>ERROR: No videos found. Can&apos;t find any videos. Is it removed?</source>
        <translation>FEL: Hittar inga videor. Ar videorna borttagna?</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="295"/>
        <location filename="../sok.cpp" line="328"/>
        <source>ERROR: No videos found. Can&apos;t find the video file.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta videofilen.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="305"/>
        <location filename="../sok.cpp" line="335"/>
        <source>ERROR: Click on &quot;Tools&quot;, &quot;Show more&quot; and run again to get more information.</source>
        <translation>FEL: Klicka på &quot;Verktyg&quot;, &quot;Visa mer&quot; och kör igen för att få mer information.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="315"/>
        <location filename="../sok.cpp" line="342"/>
        <source>ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues</source>
        <translation>FEL: Om felet kvarstår kan du rapportera det på https://github.com/spaam/svtplay-dl/issues</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="325"/>
        <location filename="../sok.cpp" line="349"/>
        <source>ERROR: Include URL and error message in problem description.</source>
        <translation>FEL: Inkludera URL och felmeddelande i problembeskrivningen.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="343"/>
        <location filename="../sok.cpp" line="361"/>
        <source>WARNING: --all-episodes not implemented for this service.</source>
        <translation>VARNING: &quot;Direkt nedladdning av alla avsnitt&quot; är inte implementerad för den här sidan.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="241"/>
        <location filename="../sok.cpp" line="367"/>
        <source>WARNING: Use program page instead of the clip / video page.</source>
        <translation>VARNING: Använd programsidan istället för klipp-/videosidan.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="204"/>
        <location filename="../listallepisodes.cpp" line="213"/>
        <location filename="../listallepisodes.cpp" line="334"/>
        <location filename="../sok.cpp" line="355"/>
        <source>ERROR: Can&apos;t find any videos.</source>
        <translation>FEL: Kan inte hitta några videor.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="472"/>
        <location filename="../sok.cpp" line="269"/>
        <source>Deviation</source>
        <translation>Avvikelse</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="268"/>
        <location filename="../sok.cpp" line="310"/>
        <source>ERROR: No videos found. Can&apos;t decode api request.</source>
        <translation>FEL: Inga videor hittades. Det går inte att avkoda api-begäran.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="67"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation>Informationen kan innehålla, eller inte innehålla:</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="31"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>Stable:</source>
        <translation>Stabila:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="28"/>
        <source>You have downloaded</source>
        <translation>Du har laddat ner</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="31"/>
        <source>Nothing</source>
        <translation>Ingenting</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="32"/>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation>Till mappen &quot;stable&quot;: Ingenting.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="34"/>
        <source>To folder &quot;stable&quot;: </source>
        <translation>Till mappen &quot;stable&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="39"/>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation>Till mappen &quot;beta&quot;: Ingenting.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="41"/>
        <source>To folder &quot;beta&quot;: </source>
        <translation>Till mappen &quot;beta&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="57"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;Information om svtplay-dl beta kan inte hittas.&lt;br&gt;Kontrollera din internetanslutning.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="81"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="96"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="252"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="273"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="290"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;Information om svtplay-dl beta kan inte hittas.&lt;br&gt;Kontrollera din internetanslutning.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="117"/>
        <source>You are not using svtplay-dl.</source>
        <translation>Du använder inte svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>Beta:</source>
        <translation>Beta:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>The latest svtplay-dl available for&lt;br&gt;download from bin.ceicer.com are</source>
        <translation>De senaste svtplay-dl från bin.ceicer.com&lt;br&gt;som är tillgängliga för nedladdning är</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="217"/>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation>&quot;Använd svtplay-dl beta&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="230"/>
        <source>&quot;Use the selected svtplay-dl&quot;</source>
        <translation>&quot;Använd vald svtplay-dl&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="336"/>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Välkommen till streamCapture2!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="344"/>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>&lt;b&gt;svtplay-dl stabila&lt;/b&gt; kan inte hittas. Du har angett en felaktig sökväg eller så har ingen sökväg angetts alls.&lt;br&gt;Eller så har filerna flyttats eller tagits bort.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="345"/>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Download stable to directory</source>
        <translation>Klicka på &lt;i&gt;Ladda ner&lt;/i&gt; och &lt;i&gt;Ladda ner stabila till mappen</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="352"/>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Download beta to directory</source>
        <translation>Klicka på &lt;i&gt;Ladda ner&lt;/i&gt; och &lt;i&gt;Ladda ner beta till mappen</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="364"/>
        <source>Click &lt;i&gt;Download&lt;/i&gt;, then click&lt;br&gt;&lt;i&gt;Download stable to directory</source>
        <translation>Klcka på &lt;i&gt;Ladda ner&lt;/i&gt;, därefter klicka på&lt;br&gt;&lt;i&gt;Ladda ner stabila till mappen</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="364"/>
        <source>&lt;/i&gt;and&lt;br&gt;&lt;i&gt;Download beta to directory</source>
        <translation>&lt;/i&gt;och&lt;br&gt;&lt;i&gt;Ladda ner beta tillmappen</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="373"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory stable&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Klicka på &quot;ladda ner&quot; och &quot;Ladda ner till mappen stable&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="351"/>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>&lt;b&gt;svtplay-dl beta&lt;/b&gt; kan inte hittas. Du har angett en felaktig sökväg eller så har ingen sökväg angets.&lt;br&gt;Eller så har filerna blivit flyttade eller borttagna.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="113"/>
        <source>You are using version:</source>
        <translation>Du använder version:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="194"/>
        <source>&quot;Use svtplay-dl stable&quot;</source>
        <translation>&quot;Använd stabila svtplay-dl&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="196"/>
        <source>&quot;Use svtplay-dl stable&quot;, but it can not be found.</source>
        <translation>&quot;Använd stabila svtplay-dl&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="215"/>
        <source>&quot;Use svtplay-dl beta&quot;</source>
        <translation>&quot;Använd svtplay-dl beta&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="382"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory beta&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Klicka på &quot;Ladda ner&quot; och Ladda ner till mappen beta&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="360"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="370"/>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;svtplay-dl stabila finns för nedladdning. &lt;/b&gt;&lt;br&gt;Version: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="361"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="379"/>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;svtplay-dl beta finns för nedladdning. &lt;/b&gt;&lt;br&gt;Version: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>You have selected</source>
        <translation>Du har valt</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="236"/>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation>svtplay-dl hittas inte i den angivna sökvägen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="232"/>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Använd vald svtplay-dl&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../download_runtime.cpp" line="27"/>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation>Bara om svtplay-dl inte fungerar behöver du installera
&quot;Microsoft Visual C++ Redistributable&quot;.
Ladda ner och dubbelklicka för att installera.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="130"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="398"/>
        <location filename="../download_runtime.cpp" line="28"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="56"/>
        <source>NFO files contain release information about the media.
No NFO file was found.</source>
        <translation>NFO-filer innehåller releaseinformation om media.
Ingen NFO-fil hittades.</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="149"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="153"/>
        <source>Episode title</source>
        <translation>Avsnitstitel</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="157"/>
        <source>Season</source>
        <translation>Säsong</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="165"/>
        <source>Plot</source>
        <translation>Handling</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="171"/>
        <source>Published</source>
        <translation>Publicerad</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="185"/>
        <source>An unexpected error occurred while downloading the file.</source>
        <translatorcomment>Ett oväntat fel inträffade när filen laddades ned.</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="217"/>
        <source>NFO Info</source>
        <translation>NFO Information</translation>
    </message>
    <message>
        <source>To the website</source>
        <translation type="vanished">Till webbsidan</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation type="vanished">Avinstallera</translation>
    </message>
    <message>
        <source>Download the new version</source>
        <translation type="vanished">Ladda ner den nya versionen</translation>
    </message>
    <message>
        <source>Download the latest version</source>
        <translation type="vanished">Ladda ner den senaste versionen</translation>
    </message>
    <message>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation type="vanished">Ett oväntat fel inträffade.&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation type="vanished">&lt;br&gt;kan inte hittas eller är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="40"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="49"/>
        <source>License streamCapture2</source>
        <translation>Licens streamCapture2</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="59"/>
        <source>License svtplay-dl</source>
        <translation>Licens svtplay-dl</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="70"/>
        <source>License FFmpeg</source>
        <translation>Licens FFmpeg</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="82"/>
        <source>License 7zip</source>
        <translation>Licens 7zip</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="99"/>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation> kunde inte hittas. Ladda ner en portable streamCapture2 där FFmpeg ingår.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="102"/>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation> kan inte hittas. Gå till &quot;Verktyg&quot;, Underhållsverktyg&quot; för att installera FFmpeg.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="104"/>
        <location filename="../about.cpp" line="108"/>
        <source>Or install </source>
        <translation>Eller installera </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="104"/>
        <location filename="../about.cpp" line="108"/>
        <source> in your system.</source>
        <translation> i ditt system.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="107"/>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation> kunde inte hittas. Ladda ner en AppImage där FFmpeg ingår.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="150"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation>svtplay-dl finns inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="152"/>
        <location filename="../about.cpp" line="156"/>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;Tools&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation> Du kan ladda ner svtplay-dl från bin.ceicer.com. Välj &quot;Verktyg&quot;, &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="155"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation>svtplay-dl.exe hittas inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="159"/>
        <source>About svtplay-dl (In the system path)</source>
        <translation>Om svtplay-dl (I systemsökvägen)</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="136"/>
        <location filename="../about.cpp" line="212"/>
        <location filename="../about.cpp" line="247"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="196"/>
        <location filename="../about.cpp" line="205"/>
        <source> cannot be found. Go to &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation> kan inte hittas. Gå till &quot;svtplay-dl&quot;, &quot;Ladda ner svtplay-dl...&quot; för att ladda ner.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="198"/>
        <location filename="../about.cpp" line="207"/>
        <source>Please click &quot;svtplay-dl&quot; and select svtplay-dl.</source>
        <translation>Klicka på &quot;svtplay-dl&quot; och välj svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="222"/>
        <source>version </source>
        <translation>version </translation>
    </message>
    <message>
        <location filename="../theme.cpp" line="40"/>
        <source>Bright Theme on next start</source>
        <translation>Ljust thema vid nästa start</translation>
    </message>
    <message>
        <location filename="../theme.cpp" line="49"/>
        <source>The program must be restarted to switch to Bright Theme.</source>
        <translation>Programmet måste startas om för att byta till ljust tema.</translation>
    </message>
    <message>
        <location filename="../theme.cpp" line="95"/>
        <source>Dark Theme on next start</source>
        <translation>Mörkt tema vid nästa start</translation>
    </message>
    <message>
        <location filename="../theme.cpp" line="104"/>
        <source>The program must be restarted to switch to Dark Theme.</source>
        <translation>Programmet måste startas om för att byta till mörkt tema.</translation>
    </message>
    <message>
        <location filename="../user_message.cpp" line="67"/>
        <location filename="../user_message.cpp" line="127"/>
        <source>Message from the developer</source>
        <translation>Meddelande från utvecklaren</translation>
    </message>
    <message>
        <location filename="../user_message.cpp" line="114"/>
        <source>There is no message.</source>
        <translation>Det finns inget meddelande.</translation>
    </message>
    <message>
        <source>The program must be restarted to change to </source>
        <translation type="vanished">Programmet måste startas om för att byta till </translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="129"/>
        <location filename="../newprg.ui" line="1123"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="481"/>
        <source>Method</source>
        <translation>Metod</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="170"/>
        <location filename="../newprg.ui" line="1150"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="817"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="35"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="74"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Klistra in länken till sidan där videon visas</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="396"/>
        <source>Quality (Bitrate)</source>
        <oldsource>Bitrate</oldsource>
        <translation>Kvalitet (Bithastighet)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="466"/>
        <source>Media streaming communications protocol.</source>
        <translation>Protokoll för att strömma media.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="530"/>
        <source>Select quality on the video you download</source>
        <translation>Välj kvalitet på videon du laddar ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="77"/>
        <location filename="../newprg.ui" line="1135"/>
        <source>Paste</source>
        <translation>Klistra in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="539"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Kvalitet (bithastighet) och metod. Högre bithastighet ger högre kvalitet och större fil.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="164"/>
        <source>Download the file you just searched for.</source>
        <translation>Ladda ner filen som du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="243"/>
        <source>Download all files you added to the list.</source>
        <translation>Ladda ner alla filer som du lagt till på listan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="575"/>
        <source>Select quality on the video you download.</source>
        <translation>Välj kvalitet på videoströmmen du laddar ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="698"/>
        <source>Select provider. If yoy need a password.</source>
        <translation>Välj en tjänsteleverantör. Om du behöver lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="735"/>
        <location filename="../newprg.ui" line="1268"/>
        <location filename="../newprg.ui" line="1361"/>
        <source>Password</source>
        <translation>Lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="830"/>
        <source>&amp;Language</source>
        <translation>S&amp;pråk</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="874"/>
        <source>&amp;Tools</source>
        <translation>V&amp;erktyg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="928"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="948"/>
        <source>&amp;Recent</source>
        <oldsource>Recent</oldsource>
        <translation>&amp;Senaste</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1054"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1063"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="878"/>
        <source>Check for updates at program start</source>
        <translation>Sök efter uppdateringar när programmet startar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="100"/>
        <source>https://</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="249"/>
        <location filename="../newprg.ui" line="1306"/>
        <source>Download all on the list</source>
        <translation>Ladda ner alla på listan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1764"/>
        <source>If Dolby Vision 4K video streams are not found, the download will fail.</source>
        <translation>Om Dolby Vision 4K-videoströmmar inte hittas kommer nedladdningen att misslyckas.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="655"/>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation>Tillåter given kvalitet att skilja sig åt med en mängd. 300 brukar fungera bra. (Bithastighet +/- 300).</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="266"/>
        <source>If Dolby Vision 4K video streams are not found, the download will probably fail.</source>
        <translation>Om Dolby Vision 4K-videoströmmar inte hittas kommer nedladdningen förmodligen att misslyckas.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="979"/>
        <source>&apos;st&apos; &amp;cookies</source>
        <translation>&apos;st&apos; &amp;cookies</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1003"/>
        <source>&amp;svtplay-dl</source>
        <translation>&amp;svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1090"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1093"/>
        <source>Exits the program.</source>
        <translation>Avsluta programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1096"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1162"/>
        <source>License streamCapture2...</source>
        <translation>Licens streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1238"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1256"/>
        <source>Create new user</source>
        <translation>Skapa ny användare</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1317"/>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation>Skapa mapp &quot;metod_kvalitet_belopp_upplösning&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1346"/>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation>Visa mer information från svtplay-dl. Visas med lila text.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1424"/>
        <source>Copy to Selected Location</source>
        <translation>Kopiera till vald plats</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1427"/>
        <source>Direct copy to the default copy location.</source>
        <translation>Direkt kopiering till förvald mapp.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1442"/>
        <source>Save the location where the finished video file is copied. If you use &quot;Direct download of all...&quot; no files are ever copied.</source>
        <translation>Spara platsen ditr den färdiga videofilen kopieras. Om du använder &quot;Direktnedladdning av alla...&quot; kopieras inga filer.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1473"/>
        <source>Add all Episodes to Download List</source>
        <translation>Lägg till alla avsnitt till nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1476"/>
        <source>Adds all episodes to the download list.</source>
        <translation>Lägger till alla avsnitt till nedladdningslistan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1861"/>
        <source>Use svtplay-dl beta</source>
        <translation>Använd svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1853"/>
        <location filename="../newprg.ui" line="1864"/>
        <source>Note that the latest stable version may be newer than the latest beta version. Check version by clicking &quot;Help&quot; -&gt; &quot;About svtplay-dl...&quot;</source>
        <translation>Observera att den senaste stabila versionen kan vara nyare än den senaste betaversionen. Kontrollera version genom att klicka &quot;Hjälp&quot;  -&gt; &quot;About svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1850"/>
        <source>Use svtplay-dl stable</source>
        <translation>Använd stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1839"/>
        <source>Use svtplay-dl from the system path</source>
        <translation>Använd svtplay-dl från systemsökvägen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1876"/>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation>Välj svtplay-dl som du har i din dator.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1884"/>
        <source>Use the selected svtplay-dl</source>
        <translation>Använd vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1887"/>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation>Använd den svtplay-dl som du valt i din dator.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1532"/>
        <source>Do not show notifications</source>
        <translation>Visa inte aviseringar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1016"/>
        <location filename="../newprg.ui" line="1019"/>
        <source>The download will fail if no subtitles are found. You can try different menu options.</source>
        <translation>Nedladdningen misslyckas om inga undertexter hittas. Du kan prova olika menyalternativ.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1022"/>
        <source>S&amp;ubtitle</source>
        <translation>&amp;Undertext</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1535"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation>Visa inte aviseringar när nedladdningen är klar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1547"/>
        <source>Create a shortcut</source>
        <translation>Skapa en genväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1558"/>
        <source>Desktop Shortcut</source>
        <translation>Skrivbordsgenväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1561"/>
        <source>Create a shortcut to streamCapture2 on the desktop.</source>
        <translation>Skapa genväg på skrivbordet till streamCapture2.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1572"/>
        <source>Create a shortcut to streamCapture2 in the operating system menu.</source>
        <translation>Skapa genväg till streamCapture2 i operativsystemets meny.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1580"/>
        <source>Useful when testing your own translation.</source>
        <translation>Användbart när du testar din egen översättning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1588"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ange ny &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1603"/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1611"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1626"/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1665"/>
        <source>License 7-Zip...</source>
        <translation>Licens 7-Zip...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1693"/>
        <source>streamCapture2 settings</source>
        <translation>Inställningar för streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1702"/>
        <source>download svtplay-dl settings</source>
        <translation>Inställningar för ladda ner svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1711"/>
        <source>NFO info</source>
        <translation>NFO information</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1714"/>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation>NFO-filer innehåller releaseinformation om media. Tillgänglig bland annat på svtplay.se.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1722"/>
        <source>Select file name</source>
        <translation>Välj filnamn</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1725"/>
        <source>You choose the name of the downloaded video file.</source>
        <translation>Du väljer namnet på den nedladdade videofilen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1734"/>
        <source>About Qt...</source>
        <translation>Om Qt...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1743"/>
        <source>Set TV4 Token</source>
        <translation>Ställ in TV4 Token</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1748"/>
        <source>How-to</source>
        <translation>Hur man gör</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1753"/>
        <source>How-to Video</source>
        <translation>Hur man gör, video</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1819"/>
        <source>Dark Theme</source>
        <translation>Mörkt tema</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1831"/>
        <source>Open...</source>
        <translation>Öppna...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1907"/>
        <location filename="../newprg.ui" line="1945"/>
        <source>Download all subtitles</source>
        <translation>Ladda ner alla undertexter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1923"/>
        <source>Merge subtitle</source>
        <translation>Slå samman undertext och video</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1934"/>
        <source>Download subtitle</source>
        <translation>Ladda ner undertext</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1953"/>
        <source>Download the subtitles in their native format</source>
        <translation>Ladda ner undertexterna i deras ursprungliga format</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1961"/>
        <source>Dont Use Native Dialogs</source>
        <translation>Använd inte operativsystemets dialogrutor</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1964"/>
        <source>Do not use the operating system&apos;s file dialog.</source>
        <translation>Använd inte operativsystemets fildialog.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1811"/>
        <source>Light Theme</source>
        <translation>Ljust tema</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1775"/>
        <source>Download subtitles</source>
        <translation>Ladda ner undertexter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1786"/>
        <source>Download subtitles and try to merge with the video file</source>
        <translation>Ladda ner undertexter och försök att sammanfoga dem med videofilen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1806"/>
        <source>If the download does not work...</source>
        <translation>Om nedladdningen inte fungerar...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1778"/>
        <location filename="../newprg.ui" line="1937"/>
        <source>The subtitle is saved in a text file (*.srt).</source>
        <translation>Undertexten sparas i en textfil (*.srt).</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1789"/>
        <location filename="../newprg.ui" line="1926"/>
        <source>If embedding in the video file does not work, try using the text file with the subtitle.</source>
        <translation>Om inbäddning i videofilen inte fungerar, försök att använda textfilen med undertexten.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1797"/>
        <location filename="../newprg.ui" line="1915"/>
        <source>No subtitles</source>
        <translation>Inga undertexter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="889"/>
        <source>Edit settings (Advanced)</source>
        <translation>Redigera inställningarna (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="272"/>
        <location filename="../newprg.ui" line="1761"/>
        <source>Dolby Vision 4K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="615"/>
        <source>Video resolution: 480p=640x480, 720p=1280x720, 1080p=1920x1080</source>
        <translation>Videoupplösning: 480p=640x480, 720p=1280x720, 1080p=1920x1080</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="995"/>
        <source>TV&amp;4</source>
        <translation>&amp;TV4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1896"/>
        <source>Download svtplay-dl...</source>
        <translation>Ladda ner svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1899"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner och dekomprimera svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1597"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1600"/>
        <source>Increase the font size.</source>
        <translation>Öka teckenstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1608"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1620"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1623"/>
        <source>Decrease the font size.</source>
        <translation>Minska teckenstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1635"/>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation>Sök efter senaste svtplay-dl från bin.ceicer.com...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1644"/>
        <location filename="../newprg.ui" line="1647"/>
        <location filename="../newprg.ui" line="1650"/>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation>Ladda ner Microsoft runtime (krävs för svtplay-dl)...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1653"/>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation>Ladda ner runtimefilen från bin.ceicer.com.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1676"/>
        <source>Check streamCapture2 for updates at start</source>
        <translation>Kolla om det finns uppdateringar till streamCapture2 vid start</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1684"/>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation>Kolla efter nya versioner av svtplay-dl vid start</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1569"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1577"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1451"/>
        <source>Select Default Download Location...</source>
        <translation>Välj standard nedladdningsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1380"/>
        <source>Direct Download of all Episodes</source>
        <translation>Direkt nedladdning av alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1383"/>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation>Försöker omedelbart att ladda ner alla avsnitt. Det går inte att skapa mappar eller välja kvalitet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1405"/>
        <source>List all Episodes</source>
        <translation>Lista alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1408"/>
        <source>Looking for video streams for all episodes.</source>
        <translation>Letar efter videoströmmar för alla avsnitt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1454"/>
        <source>Save the location for direct download.</source>
        <translation>Spara platsen för direkt nedladdning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1465"/>
        <source>Direct download to the default location.</source>
        <translation>Direktnedladdning till standardplatsen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1485"/>
        <source>Select font...</source>
        <translation>Välj teckensnitt...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1494"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation>Besök svtplay-dl forum...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1506"/>
        <source>Maintenance Tool...</source>
        <translation>Underhållsverktyg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1842"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation>Använder (om den finns) svtplay-dl i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1873"/>
        <source>Select svtplay-dl...</source>
        <translation>Välj svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1509"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Startar Underhållsverktyget. För att uppdatera eller avinstallera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1439"/>
        <source>Select Copy Location...</source>
        <translation>Välj kopieringsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1524"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Alla sparade sökningar, nedladdningslistan och alla inställningar tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1413"/>
        <location filename="../newprg.ui" line="1521"/>
        <source>Delete all settings and Exit</source>
        <translation>Ta bort alla inställningsfiler och avsluta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1416"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Alla sparade sökningar och listan med filer som ska laddas ner tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1462"/>
        <source>Download to Default Location</source>
        <translation>Ladda ner till standardplatsen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="126"/>
        <location filename="../newprg.ui" line="1126"/>
        <source>Search for video streams.</source>
        <oldsource>Search for video files.</oldsource>
        <translation>Sök efter videoströmmar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="202"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Lägg till denna videoström till listan med videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="387"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Antalet bitar som transporteras eller behandlas per tidsenhet. Högre siffror ger bättre kvalitet och större fil ..</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="953"/>
        <source>&amp;Download List</source>
        <translation>&amp;Nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="963"/>
        <source>L&amp;ogin</source>
        <translation>&amp;Logga in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="971"/>
        <source>&amp;All Episodes</source>
        <translation>Alla avsn&amp;itt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="984"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1138"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Klistra in länken till webbsidan där videon visas.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1153"/>
        <source>Download the stream you just searched for.</source>
        <oldsource>Download the file you just searched for.</oldsource>
        <translation>Ladda ner videoströmmen du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1212"/>
        <source>Look at the list of all the streams to download.</source>
        <oldsource>Look at the list of all the files to download.</oldsource>
        <translation>Titta på listan över alla videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1259"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Spara namnet på en leverantör av videoströmmar, ditt användarnamn, och om du vill, ditt lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1369"/>
        <source>Uninstall streamCapture</source>
        <translation>Avinstallera streamCapture</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1372"/>
        <source>Uninstall and remove all components</source>
        <translation>Avinstallera och ta bort alla komponenter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1388"/>
        <source>Download after Date...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1397"/>
        <source>Stop all downloads</source>
        <translation>Stoppa alla nedladdningar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1400"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Försöker stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="732"/>
        <location filename="../newprg.ui" line="1271"/>
        <location filename="../newprg.ui" line="1364"/>
        <source>If no saved password is found, click here.</source>
        <translation>Om inget lösenord är sparat, klicka här.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1294"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <oldsource>Add current video to the list of files that will be downloaded.</oldsource>
        <translation>Lägg till aktuell video till listan på videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1309"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <oldsource>Download all the files in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</oldsource>
        <translation>Ladda ner alla videoströmmar i listan. Om det är samma videoström i olika kvaliteter kommer mappar för varje videoström att skapas automatiskt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1332"/>
        <source>Edit Download List (Advanced)</source>
        <translation>Redigera nedladdningslistan (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1072"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1081"/>
        <source>Check for updates...</source>
        <translation>Sök efter uppdateringar...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1105"/>
        <source>About svtplay-dl...</source>
        <translation>Om svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1114"/>
        <source>About FFmpeg...</source>
        <translation>Om FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1171"/>
        <source>License svtplay-dl...</source>
        <translation>Licens svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1180"/>
        <source>License FFmpeg...</source>
        <translation>Licens FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1197"/>
        <source>Help...</source>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1209"/>
        <source>View Download List</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1221"/>
        <source>Delete Download List</source>
        <translation>Radera nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1247"/>
        <source>Version history...</source>
        <translation>Versionshistorik...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1279"/>
        <source>Explain what is going on</source>
        <translation>Förklar vad som händer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="208"/>
        <location filename="../newprg.ui" line="1291"/>
        <source>Add to Download List</source>
        <translation>Lägg till på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1320"/>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation>Skapa mapp &quot;metod_kvalitet_belopp&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1323"/>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation>Skapar automatiskt en mapp för varje nedladdad videoström. Om du använder &quot;Direktnedladdning av alla...&quot; skapas inga mappar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1335"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Ändra metod eller kvalitet. Ta bort en fil från nedladdning. OBS! Om du gör felaktiga ändringar kommer det inte att fungera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1343"/>
        <source>Show more</source>
        <translation>Visa mer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1185"/>
        <source>Recent files</source>
        <translation>Senaste filerna</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1229"/>
        <source>Delete download list</source>
        <translation>Ta bort nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1224"/>
        <source>All saved streams in the download list are deleted.</source>
        <oldsource>All saved items in the download list are deleted.</oldsource>
        <translation>Alla sparade videoströmmar i nedladdningslistan tas bort.</translation>
    </message>
</context>
</TS>
