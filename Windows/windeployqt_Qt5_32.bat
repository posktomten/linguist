


set "VERSION=5.15.16"
SET PATH=C:\Qt\%VERSION%\mingw810_32\bin;C:\Qt\Tools\mingw810_32\bin;%PATH%

copy C:\Qt\%VERSION%\mingw810_32\bin\linguist.exe
copy C:\Qt\%VERSION%\mingw810_32\bin\lconvert.exe
copy C:\Qt\%VERSION%\mingw810_32\bin\lprodump.exe
copy C:\Qt\%VERSION%\mingw810_32\bin\lrelease.exe
copy C:\Qt\%VERSION%\mingw810_32\bin\lrelease-pro.exe
copy C:\Qt\%VERSION%\mingw810_32\bin\lupdate.exe
copy C:\Qt\%VERSION%\mingw810_32\bin\lupdate-pro.exe

C:\Qt\%VERSION%\mingw810_32\bin\windeployqt.exe linguist.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw810_32\bin\windeployqt.exe lconvert.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw810_32\bin\windeployqt.exe lprodump.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw810_32\bin\windeployqt.exe lrelease.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw810_32\bin\windeployqt.exe lrelease-pro.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw810_32\bin\windeployqt.exe lupdate.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw810_32\bin\windeployqt.exe lupdate-pro.exe -no-translations -verbose 1
C:\Qt\5.15.16\mingw810_32\bin\windeployqt.exe linguist.exe -no-translations -verbose 1
