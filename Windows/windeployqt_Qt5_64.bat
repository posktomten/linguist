


set "VERSION=5.15.16"
SET PATH=C:\Qt\%VERSION%\mingw81_64\bin;%PATH%

copy C:\Qt\%VERSION%\mingw81_64\bin\linguist.exe
copy C:\Qt\%VERSION%\mingw81_64\bin\lconvert.exe
copy C:\Qt\%VERSION%\mingw81_64\bin\lprodump.exe
copy C:\Qt\%VERSION%\mingw81_64\bin\lrelease.exe
copy C:\Qt\%VERSION%\mingw81_64\bin\lrelease-pro.exe
copy C:\Qt\%VERSION%\mingw81_64\bin\lupdate.exe
copy C:\Qt\%VERSION%\mingw81_64\bin\lupdate-pro.exe

C:\Qt\%VERSION%\mingw81_64\bin\windeployqt.exe linguist.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw81_64\bin\windeployqt.exe lconvert.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw81_64\bin\windeployqt.exe lprodump.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw81_64\bin\windeployqt.exe lrelease.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw81_64\bin\windeployqt.exe lrelease-pro.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw81_64\bin\windeployqt.exe lupdate.exe -no-translations -verbose 1
C:\Qt\%VERSION%\mingw81_64\bin\windeployqt.exe lupdate-pro.exe -no-translations -verbose 1

