
set "VERSION=6.8.2"
set "QT=llvm-mingw_64"



copy C:\Qt\%VERSION%\%QT%\bin\linguist.exe
copy C:\Qt\%VERSION%\%QT%\bin\lconvert.exe
copy C:\Qt\%VERSION%\%QT%\bin\lprodump.exe
copy C:\Qt\%VERSION%\%QT%\bin\lrelease.exe
copy C:\Qt\%VERSION%\%QT%\bin\lrelease-pro.exe
copy C:\Qt\%VERSION%\%QT%\bin\lupdate.exe
copy C:\Qt\%VERSION%\%QT%\bin\lupdate-pro.exe


C:\Qt\%VERSION%\%QT%\bin\windeployqt6.exe linguist.exe -no-translations -verbose 1
C:\Qt\%VERSION%\%QT%\bin\windeployqt6.exe lconvert.exe -no-translations -verbose 1 
C:\Qt\%VERSION%\%QT%\bin\windeployqt6.exe lprodump.exe -no-translations -verbose 1
C:\Qt\%VERSION%\%QT%\bin\windeployqt6.exe lrelease.exe -no-translations -verbose 1
C:\Qt\%VERSION%\%QT%\bin\windeployqt6.exe lupdate.exe -no-translations -verbose 1
C:\Qt\%VERSION%\%QT%\bin\windeployqt6.exe lrelease-pro.exe -no-translations -verbose 1
C:\Qt\%VERSION%\%QT%\bin\windeployqt6.exe lupdate-pro.exe -no-translations -verbose 1
