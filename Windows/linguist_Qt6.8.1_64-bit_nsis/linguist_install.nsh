;Skapad av nsisFileList 1.3.0
;Sun Jan 5 00:48:37 2025
;Copyright (C) 2022 - 2025 Ingemar Ceicer
;Licens GNU General Public License v3.0
;https://gitlab.com/posktomten/nsisfilelist


;Add/Install Files
SetOutPath "$INSTDIR"
file "D3Dcompiler_47.dll"
file "icon.ico"
file "lconvert.exe"
file "libc++.dll"
file "libunwind.dll"
file "linguist.exe"
file "lprodump.exe"
file "lrelease.exe"
file "lrelease-pro.exe"
file "lupdate.exe"
file "lupdate-pro.exe"
file "opengl32sw.dll"
file "Qt6Core.dll"
file "Qt6Gui.dll"
file "Qt6Network.dll"
file "Qt6OpenGL.dll"
file "Qt6OpenGLWidgets.dll"
file "Qt6PrintSupport.dll"
file "Qt6Svg.dll"
file "Qt6UiTools.dll"
file "Qt6Widgets.dll"
file "unicon.ico"

SetOutPath "$INSTDIR\tls"
file "tls\qcertonlybackend.dll"
file "tls\qschannelbackend.dll"

SetOutPath "$INSTDIR\generic"
file "generic\qtuiotouchplugin.dll"

SetOutPath "$INSTDIR\iconengines"
file "iconengines\qsvgicon.dll"

SetOutPath "$INSTDIR\imageformats"
file "imageformats\qgif.dll"
file "imageformats\qicns.dll"
file "imageformats\qico.dll"
file "imageformats\qjpeg.dll"
file "imageformats\qsvg.dll"
file "imageformats\qtga.dll"
file "imageformats\qtiff.dll"
file "imageformats\qwbmp.dll"
file "imageformats\qwebp.dll"

SetOutPath "$INSTDIR\Licenses"
file "Licenses\COPYING.txt"
file "Licenses\Copyright.txt"
file "Licenses\LICENSE.FDL"
file "Licenses\LICENSE.PYTHON"
file "Licenses\ThirdPartySoftware_Listing.txt"

SetOutPath "$INSTDIR\networkinformation"
file "networkinformation\qnetworklistmanager.dll"

SetOutPath "$INSTDIR\platforms"
file "platforms\qwindows.dll"

SetOutPath "$INSTDIR\styles"
file "styles\qmodernwindowsstyle.dll"
