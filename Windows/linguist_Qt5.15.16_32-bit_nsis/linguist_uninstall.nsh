;Delete files
Delete "$INSTDIR\*.*"
Delete "$INSTDIR\iconengines\*.*"
Delete "$INSTDIR\imageformats\*.*"
Delete "$INSTDIR\Licenses\*.*"
Delete "$INSTDIR\platforms\*.*"
Delete "$INSTDIR\printsupport\*.*"
Delete "$INSTDIR\styles\*.*"

;Remove installation folders
RMDir "$INSTDIR\iconengines"
RMDir "$INSTDIR\imageformats"
RMDir "$INSTDIR\Licenses"
RMDir "$INSTDIR\platforms"
RMDir "$INSTDIR\printsupport"
RMDir "$INSTDIR\styles"
RMDir "$INSTDIR"
