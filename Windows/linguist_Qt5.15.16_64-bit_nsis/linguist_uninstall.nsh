
;Delete files
Delete "$INSTDIR\*.*"
Delete "$INSTDIR\styles\*.*"
Delete "$INSTDIR\iconengines\*.*"
Delete "$INSTDIR\imageformats\*.*"
Delete "$INSTDIR\Licenses\*.*"
Delete "$INSTDIR\platforms\*.*"
Delete "$INSTDIR\printsupport\*.*"

;Remove installation folders
RMDir "$INSTDIR\styles"
RMDir "$INSTDIR\iconengines"
RMDir "$INSTDIR\imageformats"
RMDir "$INSTDIR\Licenses"
RMDir "$INSTDIR\platforms"
RMDir "$INSTDIR\printsupport"
RMDir "$INSTDIR"

