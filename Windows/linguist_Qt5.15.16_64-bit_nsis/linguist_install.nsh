;Skapad av nsisFileList 1.3.0
;Sun Jan 5 01:39:54 2025
;Copyright (C) 2022 - 2025 Ingemar Ceicer
;Licens GNU General Public License v3.0
;https://gitlab.com/posktomten/nsisfilelist


;Add/Install Files
SetOutPath "$INSTDIR"
file "icon.ico"
file "lconvert.exe"
file "linguist.exe"
file "lprodump.exe"
file "lrelease.exe"
file "lrelease-pro.exe"
file "lupdate.exe"
file "lupdate-pro.exe"
file "Qt5Core.dll"
file "Qt5Gui.dll"
file "Qt5PrintSupport.dll"
file "Qt5Svg.dll"
file "Qt5Widgets.dll"
file "unicon.ico"

SetOutPath "$INSTDIR\styles"
file "styles\qwindowsvistastyle.dll"

SetOutPath "$INSTDIR\iconengines"
file "iconengines\qsvgicon.dll"

SetOutPath "$INSTDIR\imageformats"
file "imageformats\qgif.dll"
file "imageformats\qicns.dll"
file "imageformats\qico.dll"
file "imageformats\qjpeg.dll"
file "imageformats\qsvg.dll"
file "imageformats\qtga.dll"
file "imageformats\qtiff.dll"
file "imageformats\qwbmp.dll"
file "imageformats\qwebp.dll"

SetOutPath "$INSTDIR\Licenses"
file "Licenses\COPYING.txt"
file "Licenses\Copyright.txt"
file "Licenses\LICENSE.FDL"
file "Licenses\LICENSE.PYTHON"
file "Licenses\ThirdPartySoftware_Listing.txt"

SetOutPath "$INSTDIR\platforms"
file "platforms\qwindows.dll"

SetOutPath "$INSTDIR\printsupport"
file "printsupport\windowsprintersupport.dll"
