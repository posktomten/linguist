<!DOCTYPE html>
<html lang="en">
   <head>
     <title>Translate</title>
     <meta charset="utf-8">
<style>

img {
   width: auto;
   height: auto;
  }


body {
	display: flex;
	flex-direction: row;
	justify-content: center;
	margin: auto;
	background-color: green;
}


span {
	margin-bottom: 50px;
	font-style: italic;
	font-size: 20px;
}

.mitten {
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	padding: 30px;
	background-color: #DCDCDC;
	border-style: solid;
	border-width: 2px;
	border-color: #696969;
	border-radius: 6px;
}

h1,
h2 {
	font-family: sans-serif;
}
.os {
	background-color: white;
	display:flex;
	justify-content: center;
	font-style:normal;

}

.ingress {
 font-style: normal;
 max-width: 500px;
 background-color: white;
 padding: 10px;
 border-radius: 5px;
}
a {
	text-decoration: none;
	color:blue;
}
a:visited {

	color:blue;
}
a:hover {

	color:red;
}
code {
    font-family: monospace;
	font-style:normal;
}

@media screen and (max-width: 1200px) {
 body {
   display: block;
   background-color: #DCDCDC;
   margin: 0;
 }
 .mitten {
   padding: 10px;
   border-style: none;
   margin: 0;
 }
 .ingress {
   max-width: 100%;
 }
 .os {
   font-size: 3em;
 }

 img {
    width: 100%;
    height: auto;
   }
}


</style>

</head>
<body>
   <div class="mitten">
      <h1>Instructions for translating my (and others) <a href="https://www.qt.io/" target="_blank">Qt</a> programs</h1>
      <span class="ingress">The purpose of this project is to offer the opportunity to translate programs<br>created in the Qt
      development environment without having to download and install the entire Qt IDE.</span>
      <span class="ingress">Qt Linguist Version 5.11.1 (The latest version)</span>
      <h2>To notice</h2>
      <div><img src="bild/bild10.png" width="663" height="275" alt="bild 1"></div>
      <span>Space appears as a small point. NOTE! Sometimes the sentence can begin with a space.<br>
      Line breaks appear like the little newline sign at the end of the sentence.</span>
      <h2>Send your translation to</h2>
      <span>programmering1@ceicer.org</span>
      <h2>Here the wizard starts</h2>
      <span class="os">Instructions for Windows users</span>
      <div><img src="bild/windows.png" width="443" height="357" alt="bild 1"></div>
      <span>Download the file to translate and<br>the translation program (Windows)
      <a href="http://bin.ceicer.com/translation/install-linguist.exe">install-linguist.exe</a> (14.3 MB).<br>
      <a href="http://ceicer.org/hash/index_eng.php" target="_blank">MD5</a>: a4485ae526d933d2dc25c817b605ae0a<br>
      Double-click to start the installation.<br>
      An icon appears on the start menu after the installation is complete.<br>
      <a href="https://gitlab.com/posktomten/linguist/wikis/home" target="_blank">More info...</a></span>
      <span class="ingress">Visual C++ Redistributable MSVC++ 2017 may be required for the Windows version.<br>
      <a href="https://go.microsoft.com/fwlink/?LinkId=746572">VC_redist.x64.exe</a> (From Microsoft, 14.5 MB)<br></span>
      <span class="os">Instructions for Linux users</span>
      <div><img src="bild/linux.png" width="300" height="122" alt="bild 2"></div>
      <span>Download the file to translate and<br>the translation program (Linux) <a href="http://bin.ceicer.com/translation/linguist-x86_64.AppImage">linguist-x86_64.AppImage</a> (39.6 MB).<br>
      <a href="http://ceicer.org/hash/index_eng.php" target="_blank">MD5</a>: 9784d12759af1263393691097bd22e66<br>
      <a href="https://gitlab.com/posktomten/linguist/wikis/home" target="_blank">More info...</a></span>
      <div><img src="bild/make_executable.png" width="565" height="612" alt="bild 3"></div>
      <span>
      You must make the file executable.<br>"Right-click", select "Properties", "Permissions" and "Allow this file to run as a program".<br>
      It may look a little different in different desktop environments.<br>Or you can write <code>chmod +x linguist-x86_64.AppImage</code>
      in a terminal.<br>Double-click to start the program.</span>
      <span class="os">Instructions for all users</span>
      <div><img src="bild/bild4.png" width="651" height="820" alt="bild 4"></div>
      <span>Select "File", "Open ..." and select the file you want to translate. (* .xx_XX.ts)</span>
      <div><img src="bild/bild5.png" width="580" height="585" alt="bild 5"></div>
      <span>Here you choose to translate from one language to another language.</span>
      <div><img src="bild/bild6.png" width="580" height="585" alt="bild 6"></div>
      <span>Source language is Englis and country is United States.<br>Then select the language and country / region to translate to.</span>
      <div><img src="bild/bild7.png" width="735" height="701" alt="bild 7"></div>
      <span>You select the phrase you want to translate and enter the translation.<br>
      When you are satisfied with the translation, click on the green question mark '?',<br>
      which turns into a yellow checker.</span>
      <div><img src="bild/bild8.png" width="331" height="268" alt="bild 8"></div>
      <span>When done, select "Save" and "Release" on the "File" menu.</span>
      <div><img src="bild/bild9.png" width="357" height="185" alt="bild 9"></div>
      <span>A new file with the extension * .qm appears. Send the file * .xx_XX.ts to me,<br>and I can include it in my application!<br>
      programmering1@ceicer.org</span>
      <footer><a href="http://ceicer.org/translate/index.php" target="_blank">http://ceicer.org/translate/index.php</a></footer>
   </div>
</body>
</html>
