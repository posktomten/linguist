

<?php
   include ("../webbild/navigate2.php");
   ?>
<div class="mitten_local">
   <table class="ny">
      <tr>
         <td><a href="instructions.php" target="_blank">http://ceicer.org/instructions.php</a>
            Instructions independently, outside my website. Especially suitable for mobiles and small screens.
         </td>
      </tr>
   </table>
   <h1>Instructions for translating my<br>(and others) Qt programs</h1>
   <table class="ny">
      <tr>
         <td>The purpose of this project is to offer the opportunity to translate programs<br>created in the Qt
            development environment without having to download and install the entire Qt IDE.
         </td>
      </tr>
   </table>
   <br>&nbsp;<br>
   <h2>To notice</h2>
   <div><img src="bild/bild10.png" width="663" height="275" alt="bild 1"></div>
   <span class="in_span">Space appears as a small point. NOTE! Sometimes the sentence<br>can begin with a space.<br>
   Line breaks appear like the little newline sign at the end of the sentence.</span>
   <h2>Send your translation to</h2>
   <span class="in_span">programmering1@ceicer.org</span>
   <h2>Here the wizard starts</h2>
   <span class="os">Instructions for Windows users</span>
   <div><img src="bild/windows.png" width="443" height="357" alt="bild 1"></div>
   Double-click to start the installation.<br>
   An icon appears on the start menu after the installation is complete.<br>
   <span class="in_span">
   <a href="https://gitlab.com/posktomten/linguist/wikis/home" target="_blank">More info and download</a><br><br>
   </span>
   Visual C++ Redistributable MSVC++ 2017<br>may be required for the Windows version.<br>
   <span class="in_span">    <a href="https://go.microsoft.com/fwlink/?LinkId=746572">vc_redist.x64.exe</a> (From Microsoft, 14.5 MB)<br>
   </span>
   <span class="os">Instructions for Linux users</span>
   <div><img src="bild/linux.png" width="300" height="122" alt="bild 2"></div>
   <span class="in_span">
   <a href="https://gitlab.com/posktomten/linguist/wikis/home" target="_blank">More info and download</a></span>
   <div><img src="bild/make_executable.png" width="565" height="612" alt="bild 3"></div>
   <span class="in_span">
   You must make the file executable.<br>"Right-click", select "Properties", "Permissions"<br>and "Allow this file to run as a program".<br>
   It may look a little different in different desktop environments.<br>
   <a href="http://libreoffice.soluzioniopen.com/wp-content/uploads/2018/02/appimageExecute.gif" target="_blank">Watch how.</a>
   <br>Or you can write<br><code>chmod +x linguist-x86_64.AppImage</code><br>
   in a terminal.<br>Double-click to start the program.<br>
   </span>
   <span class="os">Instructions for all users</span>
   <div><img src="bild/bild4.png" width="651" height="820" alt="bild 4"></div>
   <span class="in_span">Select "File", "Open ..." and select the file<br>you want to translate. (* .xx_XX.ts)</span>
   <div><img src="bild/bild5.png" width="580" height="585" alt="bild 5"></div>
   <span class="in_span">Here you choose to translate from one language to another language.</span>
   <div><img src="bild/bild6.png" width="580" height="585" alt="bild 6"></div>
   <span class="in_span">Source language is Englis and country is United States.<br>Then select the language and country / region to translate to.</span>
   <div><img src="bild/bild7.png" width="735" height="701" alt="bild 7"></div>
   <span class="in_span">You select the phrase you want to translate and enter the translation.<br>
   When you are satisfied with the translation,<br>click on the green question mark '?',<br>
   which turns into a yellow checker.</span>
   <div><img src="bild/bild8.png" width="331" height="268" alt="bild 8"></div>
   <span class="in_span">When done, select "Save" and "Release" on the "File" menu.</span>
   <div><img src="bild/bild9.png" width="357" height="185" alt="bild 9"></div>
   <span class="in_span">A new file with the extension * .qm appears.<br>Send the file * .xx_XX.ts to me,<br>and I can include it in my application!<br>
   programmering1@ceicer.org</span>
</div>
</div>
</div>
</div>
</body>
</html>
